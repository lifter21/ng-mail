import { NgMailPage } from './app.po';

describe('ng-mail App', () => {
  let page: NgMailPage;

  beforeEach(() => {
    page = new NgMailPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
